# For a new app
- `ng g @nrwl/angular:app --name=kasoot`
- `ng add @ionic/angular --project=kasoot`
- `cd apps/kasoot`
- `ionic init kasoot --type=angular --default --project-id=kasoot`
- `cd ../..`
- `ionic serve` OR `ionic serve --project=kasoot`
