#!/usr/bin/env bash

APP=${1:-'laxmi-narayan-mandir'}
PLATFORM=${2:-ios}
LANE=${3:-beta}
TOKEN=TOKEN
PIPELINE=https://circleci.com/api/v2/project/bb/malikarun/monolith/pipeline
HEADER=Content-Type:application/json

curl $PIPELINE -u $TOKEN: -X POST -H $HEADER -d '{
  "parameters": {
    "app": "'$APP'",
    "platform": "'$PLATFORM'",
    "lane": "'$LANE'"
  }
}'