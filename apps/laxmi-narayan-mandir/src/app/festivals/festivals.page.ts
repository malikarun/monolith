import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { ItemDirective } from '../directives/item.directive';
import { Item } from '../models/item';
import { LoginService } from '../services/login.service';
import { AddFestivalComponent } from '../add-festival/add-festival.component';

@Component({
  selector: 'app-festivals',
  templateUrl: 'festivals.page.html',
  styleUrls: ['festivals.page.scss'],
})
export class FestivalsPageComponent extends ItemDirective {
  constructor(
    a: AngularFirestore,
    b: ModalController,
    c: AlertController,
    d: LoginService
  ) {
    super(a, b, c, d, 'festivals');
  }

  async presentModal(item: Item = null) {
    super.presentModal(item, AddFestivalComponent);
  }
}
