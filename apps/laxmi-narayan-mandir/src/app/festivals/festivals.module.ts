import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { FestivalsPageRoutingModule } from './festivals-routing.module';
import { FestivalsPageComponent } from './festivals.page';

@NgModule({
  imports: [SharedModule, FestivalsPageRoutingModule],
  declarations: [FestivalsPageComponent],
})
export class FestivalsPageModule {}
