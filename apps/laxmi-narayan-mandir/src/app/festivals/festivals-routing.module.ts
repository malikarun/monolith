import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FestivalsPageComponent } from './festivals.page';

const routes: Routes = [
  {
    path: '',
    component: FestivalsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FestivalsPageRoutingModule {}
