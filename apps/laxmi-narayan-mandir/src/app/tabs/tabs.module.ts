import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { TabsPageComponent } from './tabs.page';

@NgModule({
  imports: [SharedModule, TabsPageRoutingModule],
  declarations: [TabsPageComponent],
})
export class TabsPageModule {}
