import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { AddItemDirective } from '../directives/add-item.directive';

@Component({
  selector: 'add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss'],
})
export class AddEventComponent extends AddItemDirective {
  constructor(firestore: AngularFirestore, modal: ModalController) {
    super(firestore, modal, 'events');
  }
}
