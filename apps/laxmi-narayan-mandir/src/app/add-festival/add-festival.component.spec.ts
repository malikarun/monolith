import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddFestivalComponent } from './add-festival.component';

describe('AddFestivalComponent', () => {
  let component: AddFestivalComponent;
  let fixture: ComponentFixture<AddFestivalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
