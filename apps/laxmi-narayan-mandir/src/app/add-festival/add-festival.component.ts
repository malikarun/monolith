import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { AddItemDirective } from '../directives/add-item.directive';

@Component({
  selector: 'add-festival',
  templateUrl: './add-festival.component.html',
  styleUrls: ['./add-festival.component.scss'],
})
export class AddFestivalComponent extends AddItemDirective {
  constructor(firestore: AngularFirestore, modal: ModalController) {
    super(firestore, modal, 'festivals');
  }
}
