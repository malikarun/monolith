import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.page.html',
  styleUrls: ['settings.page.scss'],
})
export class SettingsPageComponent {
  user: Observable<firebase.User> = this.loginService.user;
  native = false;

  constructor(private loginService: LoginService, private platform: Platform) {
    this.native = this.platform.is('capacitor');
  }

  login() {
    this.loginService.signInWithFacebook();
  }

  logout() {
    this.loginService.logout();
  }
}
