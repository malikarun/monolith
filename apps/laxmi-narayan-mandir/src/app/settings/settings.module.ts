import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { SettingsPageRoutingModule } from './settings-routing.module';
import { SettingsPageComponent } from './settings.page';

@NgModule({
  imports: [SharedModule, SettingsPageRoutingModule],
  declarations: [SettingsPageComponent],
})
export class SettingsPageModule {}
