import { formatDate } from '@angular/common';
import { Directive } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Item } from '../models/item';
import { LoginService } from '../services/login.service';

@Directive()
export abstract class ItemDirective {
  items: Observable<Item[]>;
  isAdmin = this.loginService.isAdmin;
  private collection: AngularFirestoreCollection<Item>;

  constructor(
    public firestore: AngularFirestore,
    public modalController: ModalController,
    public alertController: AlertController,
    public loginService: LoginService,
    public itemKey: string
  ) {
    this.itemKey = itemKey;
    this.collection = this.firestore.collection<Item>(this.itemKey);

    this.items = this.collection.snapshotChanges().pipe(
      map((actions) =>
        actions
          .map((action) => {
            const data = action.payload.doc.data();
            const id = action.payload.doc.id;
            return { id, ...data };
          })
          .sort((a, b) => a.date.localeCompare(b.date))
      )
    );
  }

  showDate(date: string) {
    return formatDate(new Date(date), 'MMM dd, YYYY h:mm a', 'en-AU');
  }

  async presentModal(item: Item = null, component: any = null) {
    const modal = await this.modalController.create({
      component,
      componentProps: { item },
    });

    return await modal.present();
  }

  async delete(item: Item) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Delete Item!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.collection.doc(item.id).delete();
          },
        },
      ],
    });

    await alert.present();
  }
}
