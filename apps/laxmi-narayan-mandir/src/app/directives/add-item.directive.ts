import { Directive, OnInit, Input } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { Item } from '../models/item';

@Directive()
export class AddItemDirective implements OnInit {
  @Input() item: Item;
  private collection: AngularFirestoreCollection<Item>;

  constructor(
    public firestore: AngularFirestore,
    public modal: ModalController,
    public itemKey: string
  ) {
    this.itemKey = itemKey;
    this.collection = this.firestore.collection<Item>(this.itemKey);
  }

  ngOnInit(): void {
    if (!this.item) {
      this.initItem();
    }
  }

  initItem(): void {
    this.item = {
      description: '',
      name: '',
      date: '',
    };
  }

  processForm(): void {
    this.modal.dismiss();

    if (this.item.id) {
      this.collection.doc(this.item.id).set(this.item);
    } else {
      this.collection.add(this.item);
    }
  }

  dismiss() {
    this.modal.dismiss();
  }
}
