import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { Admin } from '../models/admin';
import AuthProvider = firebase.auth.AuthProvider;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public user: Observable<firebase.User> = this.afAuth.authState;
  public isAdmin: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private afAuth: AngularFireAuth,
    private firestore: AngularFirestore
  ) {
    combineLatest(
      afAuth.authState,
      this.firestore.collection('admins').valueChanges()
    ).subscribe(([user, admins]) =>
      this.isAdmin.next(
        !!admins.find((admin: Admin) => admin.uid === user?.uid)
      )
    );
  }

  signInWithFacebook() {
    this.oauthSignIn(new firebase.auth.FacebookAuthProvider());
  }

  signInWithGoogle() {
    this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.signOut();
  }

  private async oauthSignIn(provider: AuthProvider) {
    await this.afAuth.signInWithRedirect(provider);
    this.afAuth.getRedirectResult();
  }
}
