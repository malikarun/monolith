import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';

const { StatusBar: statusBar } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private platform: Platform) {
    this.initializeApp();
  }

  async initializeApp() {
    await this.platform.ready();

    if (this.platform.is('capacitor')) {
      // Set light theme for status bar
      await statusBar.setStyle({
        style: StatusBarStyle.Light,
      });
      await statusBar.hide();
      // Display content under transparent status bar (Android only)
      statusBar.setOverlaysWebView({
        overlay: true,
      });
    }
  }
}
