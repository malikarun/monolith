import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { InfoPageRoutingModule } from './info-routing.module';
import { InfoPageComponent } from './info.page';

@NgModule({
  imports: [SharedModule, InfoPageRoutingModule],
  declarations: [InfoPageComponent],
})
export class InfoPageModule {}
