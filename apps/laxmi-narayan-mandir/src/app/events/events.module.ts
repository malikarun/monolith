import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { EventsPageComponentRoutingModule } from './events-routing.module';
import { EventsPageComponent } from './events.page';

@NgModule({
  imports: [SharedModule, EventsPageComponentRoutingModule],
  declarations: [EventsPageComponent],
})
export class EventsPageComponentModule {}
