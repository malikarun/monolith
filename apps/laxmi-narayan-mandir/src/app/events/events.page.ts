import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { ItemDirective } from '../directives/item.directive';
import { Item } from '../models/item';
import { LoginService } from '../services/login.service';
import { AddEventComponent } from '../add-event/add-event.component';

@Component({
  selector: 'app-events',
  templateUrl: 'events.page.html',
  styleUrls: ['events.page.scss'],
})
export class EventsPageComponent extends ItemDirective {
  constructor(
    a: AngularFirestore,
    b: ModalController,
    c: AlertController,
    d: LoginService
  ) {
    super(a, b, c, d, 'events');
  }

  async presentModal(item: Item = null) {
    super.presentModal(item, AddEventComponent);
  }
}
