import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Media } from '@ionic-native/media/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './components/shared.module';
import { AddJokePageComponent } from './pages/tabs/entertainment/add-joke/add-joke.page';
import { AddProverbPageComponent } from './pages/tabs/entertainment/add-proverb/add-proverb.page';
import { AddWordPageComponent } from './pages/tabs/entertainment/add-word/add-word.page';
import { MandiDetailsPageComponent } from './pages/tabs/entertainment/mandi-details/mandi-details.page';
import { PoemPageComponent } from './pages/tabs/history/poem/poem.page';
import { SongPageComponent } from './pages/tabs/history/song/song.page';
import { AddRequestPageComponent } from './pages/tabs/radio/add-request/add-request.page';
import { AuthService } from './services/auth.service';
import { reduxTransFactory } from './services/translate.service';
import { effects } from './store/effects';
import { rootReducer } from './store/reducers';

@NgModule({
  declarations: [
    // Components
    AppComponent,
    // Entry Component Page
    AddRequestPageComponent,
    AddJokePageComponent,
    AddWordPageComponent,
    AddProverbPageComponent,
    PoemPageComponent,
    SongPageComponent,
    MandiDetailsPageComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    EffectsModule.forRoot(effects),
    IonicModule.forRoot(),
    StoreModule.forRoot(rootReducer),
    StoreDevtoolsModule.instrument({}),
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: reduxTransFactory,
        deps: [Store],
      },
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [
    // services
    AuthService,
    // ionic native services
    Facebook,
    Media,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
