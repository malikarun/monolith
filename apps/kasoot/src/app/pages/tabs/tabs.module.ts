import { NgModule } from '@angular/core';
import { TabsPageComponent } from './tabs.page';
import { TabsPageRoutingModule } from './tabs.router.module';
import { SharedModule } from '../../components/shared.module';

@NgModule({
  imports: [SharedModule, TabsPageRoutingModule],
  declarations: [TabsPageComponent],
})
export class TabsPageModule {}
