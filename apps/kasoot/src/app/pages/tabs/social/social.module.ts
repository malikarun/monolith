import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../components/shared.module';
import { SocialPageComponent } from './social.page';

const routes: Routes = [{ path: '', component: SocialPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [SocialPageComponent],
})
export class SocialPageModule {}
