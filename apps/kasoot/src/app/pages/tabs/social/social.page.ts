import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Capacitor } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ITranslatedValues } from '../../../../app/interfaces';
import { RatingService } from '../../../../app/services/rating.service';
import { GlobalService } from '../../../services/global.service';
import { SocialSharingService } from '../../../services/social-sharing.service';

@Component({
  selector: 'kasoot-social',
  templateUrl: 'social.page.html',
  styleUrls: ['social.page.scss'],
})
export class SocialPageComponent {
  public isNativeDevice: boolean;
  public newsWebsiteUrl!: string;

  constructor(
    private ratingService: RatingService,
    private database: AngularFireDatabase,
    private socialSharingService: SocialSharingService,
    private globalService: GlobalService,
    private alertCtrl: AlertController,
    private translateService: TranslateService
  ) {
    this.isNativeDevice = this.isNative;

    // define variables after the call
    this.database
      .object('server')
      .valueChanges()
      .subscribe((server: any) => {
        this.newsWebsiteUrl = server.newsWebsiteUrl;
      });
  }

  openNewsSite(): void {
    this.globalService.openInBrowser(this.newsWebsiteUrl);
  }

  rate(): void {
    this.ratingService.setAppRating(true);
  }

  openFacebook() {
    const scheme = this.isIos ? 'fb://' : 'com.facebook.katana';
    const visitUrl = this.isIos
      ? 'fb://profile/331985113591743'
      : 'fb://page/331985113591743';
    const websiteUrl = 'https://facebook.com/radiokasoot/';
    this.globalService.openSystemORBrowser(scheme, visitUrl, websiteUrl);
  }

  openTwitter() {
    const scheme = this.isIos ? 'twitter://' : 'com.twitter.android';
    const visitUrl = 'twitter://user?screen_name=radiokasoot';
    const websiteUrl = 'https://twitter.com/radiokasoot/';
    this.globalService.openSystemORBrowser(scheme, visitUrl, websiteUrl);
  }

  openInstagram() {
    const scheme = this.isIos ? 'instagram://' : 'com.instagram.android';
    const visitUrl = 'instagram://user?username=radiokasoot';
    const websiteUrl = 'https://instagram.com/radiokasoot/';
    this.globalService.openSystemORBrowser(scheme, visitUrl, websiteUrl);
  }

  openGooglePlus() {
    const scheme = this.isIos ? 'google-plus://' : 'com.gplus.android';
    const visitUrl = 'gplus://plus.google.com/+RadioKasoot';
    const websiteUrl = 'https://plus.google.com/+RadioKasoot';
    this.globalService.openSystemORBrowser(scheme, visitUrl, websiteUrl);
  }

  openYoutubeSub() {
    const scheme = 'vnd.youtube://';
    const visitUrl = 'vnd.youtube://user?username=radiokasoot';
    const websiteUrl = 'https://www.youtube.com/radiokasoot';

    this.globalService.openSystemORBrowser(scheme, visitUrl, websiteUrl);
  }

  share(linkType: string): void {
    this.socialSharingService.shareStoreLink(linkType);
  }

  sharePopup(): void {
    const translationKeys: string[] = [
      'ALERTS.SHARE_LINKS',
      'ALERTS.FACEBOOK_LINK',
      'ALERTS.ANDROID_APP_LINK',
      'ALERTS.IOS_APP_LINK',
      'ALERTS.SHARE_LINKS_YES',
      'ALERTS.SHARE_LINKS_NO',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: ITranslatedValues) => {
        const header = values['ALERTS.SHARE_LINKS'],
          fbLink = values['ALERTS.FACEBOOK_LINK'],
          androidLink = values['ALERTS.ANDROID_APP_LINK'],
          iOSLink = values['ALERTS.IOS_APP_LINK'],
          shareYes = values['ALERTS.SHARE_LINKS_YES'],
          shareNo = values['ALERTS.SHARE_LINKS_NO'];

        const alert = await this.alertCtrl.create({
          header,
          inputs: [
            {
              type: 'radio',
              label: fbLink,
              value: '',
              checked: !this.isNative,
            },
            {
              type: 'radio',
              label: androidLink,
              value: 'android',
              checked: this.isAndroid,
            },
            {
              type: 'radio',
              label: iOSLink,
              value: 'ios',
              checked: this.isIos,
            },
          ],
          buttons: [
            {
              text: shareNo,
              role: 'cancel',
            },
            {
              text: shareYes,
              handler: (link) => this.share(link),
            },
          ],
        });

        await alert.present();
      });
  }

  private get isIos() {
    return Capacitor.getPlatform() === 'ios';
  }

  private get isAndroid() {
    return Capacitor.getPlatform() === 'android';
  }

  private get isNative() {
    return Capacitor.isNativePlatform();
  }
}
