import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../components/shared.module';
import { SettingsPageComponent } from './settings.page';

const routes: Routes = [{ path: '', component: SettingsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [SettingsPageComponent],
})
export class SettingsPageModule {}
