import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { ITranslatedValues } from '../../../../app/interfaces';
import { Capacitor } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from '../../../services/global.service';
import { LanguageService } from '../../../services/language.service';
import { PlayerTypeService } from '../../../services/player-type.service';
import { login, logout } from '../../../store/actions/auth.actions';
import { IAuth } from '../../../store/models/auth.model';
import { ILanguage } from '../../../store/models/language.model';
import { IPlayerType } from '../../../store/models/player-type.model';
import { IServer } from '../../../store/models/player.model';
import { IAppState } from '../../../store/state';

@Component({
  selector: 'kasoot-settings',
  templateUrl: 'settings.page.html',
  styleUrls: ['settings.page.scss'],
})
export class SettingsPageComponent {
  auth!: IAuth;
  currentLang!: ILanguage;
  currentPlayer!: IPlayerType;
  isNativeDevice: boolean;

  constructor(
    private alertCtrl: AlertController,
    private database: AngularFireDatabase,
    private globalService: GlobalService,
    private languageService: LanguageService,
    private playerService: PlayerTypeService,
    private translateService: TranslateService,
    private store: Store<IAppState>
  ) {
    this.isNativeDevice = Capacitor.isNativePlatform();

    this.store.select('auth').subscribe((auth) => (this.auth = auth));

    this.store
      .select('languages')
      .subscribe((languages) => (this.currentLang = languages.current));

    this.store
      .select('playerTypes')
      .subscribe((playerTypes) => (this.currentPlayer = playerTypes.current));
  }

  visitWebsite(): void {
    this.database
      .object('server')
      .valueChanges()
      .subscribe((server: any) =>
        this.globalService.openInBrowser(server.websiteUrl)
      );
  }

  facebookLogin() {
    this.store.dispatch(login());
  }

  selectLanguage() {
    this.languageService.langPopup();
  }

  selectPlayer() {
    this.playerService.playerTypePopup();
  }

  openEmail(): void {
    const translationKeys: Array<string> = [
      'ALERTS.EMAIL_BEFORE_SEND_TITLE',
      'ALERTS.EMAIL_BEFORE_SEND_MESSAGE',
      'ALERTS.EMAIL_BEFORE_SEND_AGREE',
      'ALERTS.EMAIL_BEFORE_SEND_DISAGREE',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: ITranslatedValues) => {
        const header: string = values['ALERTS.EMAIL_BEFORE_SEND_TITLE'],
          message: string = values['ALERTS.EMAIL_BEFORE_SEND_MESSAGE'],
          sendEmail: string = values['ALERTS.EMAIL_BEFORE_SEND_AGREE'],
          dontSendEmail: string = values['ALERTS.EMAIL_BEFORE_SEND_DISAGREE'];

        const alert = await this.alertCtrl.create({
          header,
          message,
          buttons: [
            {
              text: dontSendEmail,
              role: 'cancel',
            },
            {
              text: sendEmail,
              handler: (_) =>
                this.globalService.openInBrowser(
                  'mailto:radiokasoot@gmail.com'
                ),
            },
          ],
        });

        await alert.present();
      });
  }

  logout(): void {
    const translationKeys: Array<string> = [
      'ALERTS.LOGOUT_TITLE',
      'ALERTS.LOGOUT_MESSAGE',
      'ALERTS.LOGOUT_AGREE',
      'ALERTS.LOGOUT_DISAGREE',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: ITranslatedValues) => {
        const header: string = values['ALERTS.LOGOUT_TITLE'],
          message: string = values['ALERTS.LOGOUT_MESSAGE'],
          logoutText: string = values['ALERTS.LOGOUT_AGREE'],
          dontLogout: string = values['ALERTS.LOGOUT_DISAGREE'];

        const alert = await this.alertCtrl.create({
          header,
          message,
          buttons: [
            {
              text: dontLogout,
              role: 'cancel',
            },
            {
              text: logoutText,
              handler: (_) => this.store.dispatch(logout()),
            },
          ],
        });

        await alert.present();
      });
  }
}
