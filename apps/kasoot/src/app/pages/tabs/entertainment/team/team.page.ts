import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GlobalService } from '../../../../services/global.service';
import { getTeam } from '../../../../store/actions/team.actions';
import { ITeam } from '../../../../store/models/team.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-team',
  templateUrl: './team.page.html',
  styleUrls: ['./team.page.scss'],
})
export class TeamPageComponent {
  public team$!: Observable<ITeam>;

  constructor(
    private globalService: GlobalService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getTeam());
  }

  ionViewDidEnter() {
    this.team$ = this.store.select('team');
  }

  openOnFacebook(url: string) {
    const numberPattern = /\d+/g;
    const visitUrl = url.match(numberPattern)?.[0];
    const websiteUrl = `https://facebook.com/${visitUrl}`;
    this.globalService.openInBrowser(websiteUrl);
  }
}
