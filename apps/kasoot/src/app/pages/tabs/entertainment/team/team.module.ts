import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { TeamPageComponent } from './team.page';

const routes: Routes = [{ path: '', component: TeamPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [TeamPageComponent],
})
export class TeamPageModule {}
