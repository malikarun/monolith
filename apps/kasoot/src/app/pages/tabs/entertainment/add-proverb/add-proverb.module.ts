import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { AddProverbPageComponent } from './add-proverb.page';

const routes: Routes = [{ path: '', component: AddProverbPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [AddProverbPageComponent],
})
export class AddProverbPageModule {}
