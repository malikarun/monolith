import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'kasoot-entertainment',
  templateUrl: 'entertainment.page.html',
  styleUrls: ['entertainment.page.scss'],
})
export class EntertainmentPageComponent {
  constructor(private router: Router) {}

  navigate(url: string) {
    this.router.navigateByUrl(url);
  }
}
