import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherPageComponent } from './weather.page';
import { SharedModule } from '../../../../components/shared.module';

const routes: Routes = [{ path: '', component: WeatherPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [WeatherPageComponent],
})
export class WeatherPageModule {}
