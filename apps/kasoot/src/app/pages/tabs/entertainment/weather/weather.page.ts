import { Component } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { Store } from '@ngrx/store';
import {
  hideLoader,
  showLoader,
} from '../../../../store/actions/loader.actions';
import {
  getWeather,
  getWeatherDescription,
} from '../../../../store/actions/weather.actions';
import { IWeatherContainer } from '../../../../store/models/weather.model';
import { IAppState } from '../../../../store/state';
import { get } from 'lodash';

@Component({
  selector: 'kasoot-weather',
  templateUrl: './weather.page.html',
  styleUrls: ['./weather.page.scss'],
})
export class WeatherPageComponent {
  icon!: string;
  openWeatherMapIconUrl!: string;
  weatherData!: IWeatherContainer;
  locationError = true;
  isDayTime = true;
  isLoading = true;

  constructor(private store: Store<IAppState>) {
    const hours = new Date().getHours();
    this.isDayTime = hours > 6 && hours < 19;
  }

  async ionViewDidEnter() {
    this.store.select('weather').subscribe((weather: IWeatherContainer) => {
      this.weatherData = weather;

      if (this.weatherData && !this.weatherData.info) {
        const code = get(this.weatherData, 'data.weather.0.id');
        this.store.dispatch(getWeatherDescription({ code }));
      }
    });

    try {
      this.store.dispatch(showLoader());
      const position = await Geolocation.getCurrentPosition();
      this.locationError = false;
      this.isLoading = false;
      this.store.dispatch(hideLoader());
      this.store.dispatch(
        getWeather({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        })
      );
    } catch (error) {
      this.locationError = true;
      this.isLoading = false;
    }
  }

  getIcon(): string {
    const icon = get(this.weatherData, 'data.weather.0.icon');
    return icon && `./assets/weather/${icon}.png`;
  }
}
