import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { AddJokePageComponent } from './add-joke.page';

const routes: Routes = [{ path: '', component: AddJokePageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [AddJokePageComponent],
})
export class AddJokePageModule {}
