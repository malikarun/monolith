import { Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { ITranslatedValues } from '../../../../interfaces';
import { AuthService } from '../../../../services/auth.service';
import { createJoke } from '../../../../store/actions/joke.actions';
import { IJoke } from '../../../../store/models/joke.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-add-joke',
  templateUrl: './add-joke.page.html',
  styleUrls: ['./add-joke.page.scss'],
})
export class AddJokePageComponent {
  public joke!: IJoke;
  public editorConfig!: Record<string, unknown>;

  constructor(
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public authService: AuthService,
    private translateService: TranslateService,
    private store: Store<IAppState>
  ) {}

  ionViewDidEnter() {
    this.editorConfig = {
      items: [
        'Bold',
        'Italic',
        'Underline',
        '-',
        'ClearFormat',
        '-',
        'OrderedList',
        'UnorderedList',
        '-',
        'Formats',
        '-',
      ],
    };

    this.store.pipe(take(1)).subscribe((state: IAppState) => {
      this.joke = {
        approved: false,
        username: state.auth.displayName,
        title: '',
      };
    });
  }

  saveJoke() {
    if (this.joke.title) {
      this.store.dispatch(createJoke({ payload: this.joke }));
      this.dismiss();
    } else {
      this.showError();
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  showError() {
    const translationKeys: Array<string> = [
      'ALERTS.JOKE_ERROR_TITLE',
      'ALERTS.JOKE_ERROR_MESSAGE',
      'ALERTS.JOKE_ERROR_BUTTON',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: ITranslatedValues) => {
        const header = values['ALERTS.JOKE_ERROR_TITLE'],
          message = values['ALERTS.JOKE_ERROR_MESSAGE'],
          button = values['ALERTS.JOKE_ERROR_BUTTON'];

        const alert = await this.alertCtrl.create({
          header,
          message,
          buttons: [button],
        });

        await alert.present();
      });
  }
}
