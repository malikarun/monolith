import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { AddWordPageComponent } from './add-word.page';

const routes: Routes = [{ path: '', component: AddWordPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [AddWordPageComponent],
})
export class AddWordPageModule {}
