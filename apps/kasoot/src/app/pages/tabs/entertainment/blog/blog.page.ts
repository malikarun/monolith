import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BlogService } from '../../../../services/blog.service';
import { getBlogPost } from '../../../../store/actions/blog.actions';
import { IBlog } from '../../../../store/models/blog.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-blog',
  templateUrl: './blog.page.html',
  styleUrls: ['./blog.page.scss'],
})
export class BlogPageComponent {
  public post$!: Observable<IBlog>;
  @ViewChild(IonContent, { static: true }) content: IonContent | undefined;

  constructor(
    public blogService: BlogService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getBlogPost({ token: '' }));
  }

  ionViewDidEnter() {
    this.post$ = this.store.select('blog');
  }

  getNextPost(token: string) {
    this.store.dispatch(getBlogPost({ token }));
    (this.content as IonContent).scrollToTop();
  }
}
