import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { BlogPageComponent } from './blog.page';

const routes: Routes = [{ path: '', component: BlogPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [BlogPageComponent],
})
export class BlogPageModule {}
