import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { ProverbsPageComponent } from './proverbs.page';

const routes: Routes = [{ path: '', component: ProverbsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ProverbsPageComponent],
})
export class ProverbsPageModule {}
