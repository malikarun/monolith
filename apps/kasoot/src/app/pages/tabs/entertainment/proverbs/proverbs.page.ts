import { Component } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';
import { SocialSharingService } from '../../../../services/social-sharing.service';
import {
  getFirstProverb,
  getNextProverb,
  getPreviousProverb,
} from '../../../../store/actions/proverb.actions';
import { IAuth } from '../../../../store/models/auth.model';
import {
  IProverb,
  IProverbContainer,
} from '../../../../store/models/proverb.model';
import { IAppState } from '../../../../store/state';
import { Capacitor } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AddProverbPageComponent } from '../add-proverb/add-proverb.page';

@Component({
  selector: 'kasoot-proverbs',
  templateUrl: './proverbs.page.html',
  styleUrls: ['./proverbs.page.scss'],
})
export class ProverbsPageComponent {
  public isNativeDevice = false;
  public container$!: Observable<IProverbContainer>;
  public auth$!: Observable<IAuth>;
  public showAnimation!: boolean;

  constructor(
    public authService: AuthService,
    private modalCtrl: ModalController,
    private socialSharingService: SocialSharingService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getFirstProverb());
  }

  ionViewDidEnter() {
    this.isNativeDevice = Capacitor.isNativePlatform();
    this.container$ = this.store.select('proverbs');
    this.auth$ = this.store.select('auth');
    this.store.select('proverbs').subscribe(() => (this.showAnimation = true));
  }

  handleAddProverb() {
    this.auth$.subscribe((auth) =>
      auth.loggedIn
        ? this.presentProverbModal()
        : this.authService.promptForLogin()
    );
  }

  async presentProverbModal() {
    const addProverbModal = await this.modalCtrl.create({
      component: AddProverbPageComponent,
    });

    await addProverbModal.present();
  }

  nextProverb(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getNextProverb({ key }));
  }

  previousProverb(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getPreviousProverb({ key }));
  }

  shareProverb(proverb: IProverb) {
    this.socialSharingService.shareProverb(proverb);
  }
}
