import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Dictionary, groupBy } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getMandiPrices } from '../../../../store/actions/mandi.actions';
import { IRecord } from '../../../../store/models/mandi.model';
import { IAppState } from '../../../../store/state';
import { MandiDetailsPageComponent } from '../mandi-details/mandi-details.page';

const groupByName = (records: IRecord[]) =>
  groupBy(records, (record) => record.market);

@Component({
  selector: 'kasoot-mandi',
  templateUrl: './mandi.page.html',
  styleUrls: ['./mandi.page.scss'],
})
export class MandiPageComponent {
  groups$!: Observable<Dictionary<IRecord[]>>;

  constructor(
    private modalCtrl: ModalController,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getMandiPrices());
  }

  ionViewDidEnter() {
    this.groups$ = this.store
      .select('mandi')
      .pipe(map((mandi: any) => groupByName(mandi.records)));
  }

  async presentMandiDetailsModal(records: IRecord[], title: string) {
    const poemModal = await this.modalCtrl.create({
      component: MandiDetailsPageComponent,
      componentProps: { records, title },
    });

    await poemModal.present();
  }
}
