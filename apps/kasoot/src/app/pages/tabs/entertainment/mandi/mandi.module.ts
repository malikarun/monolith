import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { MandiPageComponent } from './mandi.page';

const routes: Routes = [{ path: '', component: MandiPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [MandiPageComponent],
})
export class MandiPageModule {}
