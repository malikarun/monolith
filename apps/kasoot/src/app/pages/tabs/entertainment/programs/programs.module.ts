import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { ProgramsPageComponent } from './programs.page';

const routes: Routes = [{ path: '', component: ProgramsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ProgramsPageComponent],
})
export class ProgramsPageModule {}
