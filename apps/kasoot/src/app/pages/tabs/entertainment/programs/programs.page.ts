import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getProgram } from '../../../../store/actions/program.actions';
import { IProgram } from '../../../../store/models/program.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-programs',
  templateUrl: './programs.page.html',
  styleUrls: ['./programs.page.scss'],
})
export class ProgramsPageComponent {
  public programs$!: Observable<IProgram[]>;

  constructor(private store: Store<IAppState>) {
    this.store.dispatch(getProgram());
  }

  ionViewDidEnter() {
    this.programs$ = this.store.select('programs');
  }
}
