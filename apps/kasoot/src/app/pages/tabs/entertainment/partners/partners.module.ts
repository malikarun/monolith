import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { PartnersPageComponent } from './partners.page';

const routes: Routes = [{ path: '', component: PartnersPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [PartnersPageComponent],
})
export class PartnersPageModule {}
