import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GlobalService } from '../../../../services/global.service';
import { getPartners } from '../../../../store/actions/partner.actions';
import { IPartner } from '../../../../store/models/partner.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-partners',
  templateUrl: './partners.page.html',
  styleUrls: ['./partners.page.scss'],
})
export class PartnersPageComponent {
  public partners$!: Observable<IPartner[]>;

  constructor(
    private globalService: GlobalService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getPartners());
  }

  ionViewDidEnter() {
    this.partners$ = this.store.select('partners');
  }

  openInBrowser(url: string): void {
    this.globalService.openInBrowser(url);
  }
}
