import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../components/shared.module';
import { EntertainmentPageComponent } from './entertainment.page';

const routes: Routes = [{ path: '', component: EntertainmentPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [EntertainmentPageComponent],
})
export class EntertainmentPageModule {}
