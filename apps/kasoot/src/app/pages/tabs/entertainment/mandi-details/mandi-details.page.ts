import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IRecord } from '../../../../store/models/mandi.model';

@Component({
  selector: 'kasoot-mandi-details',
  templateUrl: './mandi-details.page.html',
  styleUrls: ['./mandi-details.page.scss'],
})
export class MandiDetailsPageComponent {
  @Input() title!: string;
  @Input() records!: IRecord[];

  constructor(private modalCtrl: ModalController) {}

  dismiss() {
    this.modalCtrl.dismiss();
  }
}
