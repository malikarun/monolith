import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { MandiDetailsPageComponent } from './mandi-details.page';

const routes: Routes = [{ path: '', component: MandiDetailsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [MandiDetailsPage]
})
export class MandiDetailsPageModule {}
