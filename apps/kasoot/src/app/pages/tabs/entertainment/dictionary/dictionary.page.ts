import { Component } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthService } from '../../../../services/auth.service';
import { SocialSharingService } from '../../../../services/social-sharing.service';
import {
  getFirstWord,
  getNextWord,
  getPreviousWord,
} from '../../../../store/actions/dictionary.actions';
import { IAuth } from '../../../../store/models/auth.model';
import { IDictionary, IWord } from '../../../../store/models/dictionary.model';
import { IAppState } from '../../../../store/state';
import { AddWordPageComponent } from '../add-word/add-word.page';

@Component({
  selector: 'kasoot-dictionary',
  templateUrl: './dictionary.page.html',
  styleUrls: ['./dictionary.page.scss'],
})
export class DictionaryPageComponent {
  public dictionary$!: Observable<IDictionary>;
  public auth$!: Observable<IAuth>;
  public isNativeDevice = false;
  public showAnimation!: boolean;

  constructor(
    public authService: AuthService,
    private modalCtrl: ModalController,
    private socialSharingService: SocialSharingService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getFirstWord());
  }

  ionViewDidEnter() {
    this.isNativeDevice = Capacitor.isNativePlatform();
    this.dictionary$ = this.store.select('dictionary');
    this.auth$ = this.store.select('auth');
    this.store
      .select('dictionary')
      .subscribe(() => (this.showAnimation = true));
  }

  handleAddWord() {
    this.auth$.subscribe((auth) =>
      auth.loggedIn
        ? this.presentWordModal()
        : this.authService.promptForLogin()
    );
  }

  async presentWordModal() {
    const addWordModal = await this.modalCtrl.create({
      component: AddWordPageComponent,
    });
    await addWordModal.present();
  }

  nextWord(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getNextWord({ key }));
  }

  previousWord(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getPreviousWord({ key }));
  }

  shareWord(word: IWord) {
    this.socialSharingService.shareWord(word);
  }
}
