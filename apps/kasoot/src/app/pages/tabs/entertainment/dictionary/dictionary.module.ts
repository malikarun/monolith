import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { DictionaryPageComponent } from './dictionary.page';

const routes: Routes = [{ path: '', component: DictionaryPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [DictionaryPageComponent],
})
export class DictionaryPageModule {}
