import { Component } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthService } from '../../../../services/auth.service';
import { SocialSharingService } from '../../../../services/social-sharing.service';
import {
  getFirstJoke,
  getNextJoke,
  getPreviousJoke,
} from '../../../../store/actions/joke.actions';
import { IAuth } from '../../../../store/models/auth.model';
import { IJoke, IJokeContainer } from '../../../../store/models/joke.model';
import { IAppState } from '../../../../store/state';
import { AddJokePageComponent } from '../add-joke/add-joke.page';

@Component({
  selector: 'kasoot-jokes',
  templateUrl: './jokes.page.html',
  styleUrls: ['./jokes.page.scss'],
})
export class JokesPageComponent {
  public isNativeDevice = false;
  public container$!: Observable<IJokeContainer>;
  public auth$!: Observable<IAuth>;
  public showAnimation!: boolean;

  constructor(
    public authService: AuthService,
    private modalCtrl: ModalController,
    private socialSharingService: SocialSharingService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getFirstJoke());
  }

  ionViewDidEnter() {
    this.isNativeDevice = Capacitor.isNativePlatform();
    this.container$ = this.store.select('jokes');
    this.auth$ = this.store.select('auth');
    this.store.select('jokes').subscribe(() => (this.showAnimation = true));
  }

  // TODO unsubscribe
  handleAddJoke() {
    this.auth$.subscribe((auth) =>
      auth.loggedIn
        ? this.presentJokeModal()
        : this.authService.promptForLogin()
    );
  }

  async presentJokeModal() {
    const addJokeModal = await this.modalCtrl.create({
      component: AddJokePageComponent,
    });

    await addJokeModal.present();
  }

  nextJoke(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getNextJoke({ key }));
  }

  previousJoke(key: string) {
    this.showAnimation = false;
    this.store.dispatch(getPreviousJoke({ key }));
  }

  shareJoke(joke: IJoke) {
    this.socialSharingService.shareJoke(joke);
  }
}
