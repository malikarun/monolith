import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { JokesPageComponent } from './jokes.page';

const routes: Routes = [{ path: '', component: JokesPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [JokesPageComponent],
})
export class JokesPageModule {}
