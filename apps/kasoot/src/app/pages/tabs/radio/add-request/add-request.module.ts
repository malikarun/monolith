import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { AddRequestPageComponent } from './add-request.page';

const routes: Routes = [{ path: '', component: AddRequestPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [AddRequestPage]
})
export class AddRequestPageModule {}
