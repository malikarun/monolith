import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Capacitor } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { interval, Subscription } from 'rxjs';
import { MusicService } from '../../../services/music.service';
import { getServer, getSongTitle } from '../../../store/actions/player.actions';
import { IPlayer, IServer, IStream } from '../../../store/models/player.model';
import { IAppState } from '../../../store/state';

@Component({
  selector: 'kasoot-radio',
  templateUrl: 'radio.page.html',
  styleUrls: ['radio.page.scss'],
})
export class RadioPageComponent {
  public player!: IPlayer;
  public currentTime!: string;
  private server!: IServer;
  private intervalSubscription!: Subscription;
  private alert!: HTMLIonAlertElement;

  constructor(
    private musicService: MusicService,
    private alertCtrl: AlertController,
    private router: Router,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getServer());

    this.store.select('player')?.subscribe((player: IPlayer) => {
      this.player = player;
      if (player) {
        this.server = player.server;
        this.musicService.setSrc(player.audioSrc);

        if (!this.player.isPlaying) {
          this.stopTimer();
        } else {
          this.startTimer();
        }
      }
    });
  }

  ionViewDidEnter() {
    setTimeout(() => this.getSongInfo(), 5000);

    // register back button action
    if (Capacitor.isNativePlatform()) {
      this.registerBackButton();
    }
  }

  playPause(isPlaying: boolean) {
    this.musicService.play(!isPlaying, true);
  }

  navigate(url: string) {
    this.router.navigateByUrl(url);
  }

  getSongInfo() {
    interval(10000).subscribe(this.setSongTitle);
  }

  startTimer() {
    this.stopTimer();
    this.intervalSubscription = interval(1000).subscribe(this.setTime);
  }

  stopTimer() {
    if (this.intervalSubscription) {
      this.intervalSubscription.unsubscribe();
    }
  }

  selectStream(stream: IStream) {
    this.stopTimer();
    this.musicService.selectStream(stream);
  }

  isCurrentStream(stream: IStream) {
    return stream === this.player.currentStream;
  }

  // register an event when back button is pressed on android
  registerBackButton() {
    App.addListener(
      'backButton',
      () => this.router.url === '/tabs/radio' && this.showExit()
    );
  }

  private setSongTitle = () =>
    this.server &&
    this.store.dispatch(getSongTitle({ statsUrl: this.server.statsUrl }));

  private setTime = (seconds: number) => {
    const date = new Date();
    date.setSeconds(seconds);
    this.currentTime = date.toISOString().substr(11, 8);
  };

  async showExit() {
    if (this.alert) {
      this.alert.dismiss();
    }

    this.alert = await this.alertCtrl.create({
      header: 'के बात होग्यी?',
      message: 'बंद होजागा इस्ते, यो ना दाब्या करो',
      buttons: [
        { text: 'बाजण दे' },
        {
          text: 'बंद कर दे',
          handler: () => (navigator as any).app.exitApp(),
        },
      ],
    });

    this.alert.present();
  }
}
