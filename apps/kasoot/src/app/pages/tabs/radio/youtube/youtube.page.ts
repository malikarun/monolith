import { Component, ViewChild } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GlobalService } from '../../../../services/global.service';
import { SocialSharingService } from '../../../../services/social-sharing.service';
import { getYoutube } from '../../../../store/actions/youtube.actions';
import { IYoutube, IYoutubePost } from '../../../../store/models/youtube.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-youtube',
  templateUrl: './youtube.page.html',
  styleUrls: ['./youtube.page.scss'],
})
export class YoutubePageComponent {
  @ViewChild(IonInfiniteScroll)
  infiniteScroll!: IonInfiniteScroll;
  public isNativeDevice = false;
  public youtube$!: Observable<IYoutube>;

  constructor(
    private socialSharingService: SocialSharingService,
    private globalService: GlobalService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getYoutube({ payload: '' }));
  }

  ionViewDidEnter() {
    this.isNativeDevice = Capacitor.isNativePlatform();
    this.youtube$ = this.store.select('youtube');
  }

  fetchData(token: string): void {
    this.store.dispatch(getYoutube({ payload: token }));
    this.infiniteScroll.complete();
  }

  playVideo(post: IYoutubePost): void {
    this.globalService.openInBrowser(
      `https://youtube.com/watch?v=${post?.id?.videoId}`
    );
  }

  loadMore(_: any, token: string): void {
    this.fetchData(token);
  }

  shareVideo(post: IYoutubePost) {
    this.socialSharingService.shareVideo(post);
  }
}
