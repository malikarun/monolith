import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { YoutubePageComponent } from './youtube.page';

const routes: Routes = [{ path: '', component: YoutubePageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [YoutubePageComponent],
})
export class YoutubePageModule {}
