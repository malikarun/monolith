import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../components/shared.module';
import { RadioPageComponent } from './radio.page';

const routes: Routes = [{ path: '', component: RadioPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [RadioPageComponent],
})
export class RadioPageModule {}
