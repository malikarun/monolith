import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { RequestsPageComponent } from './requests.page';

const routes: Routes = [{ path: '', component: RequestsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [RequestsPageComponent],
})
export class RequestsPageModule {}
