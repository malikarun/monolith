import { Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { ITranslatedValues } from '../../../../interfaces';
import { getRequests } from '../../../../store/actions/request.actions';
import { getRequestsAreTaken } from '../../../../store/actions/requests-are-taken.actions';
import { IRequest } from '../../../../store/models/request.model';
import { IRequestsAreTaken } from '../../../../store/models/requests-are-taken.model';
import { IAppState } from '../../../../store/state';
import { AddRequestPageComponent } from '../add-request/add-request.page';

@Component({
  selector: 'kasoot-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPageComponent {
  public requests$!: Observable<IRequest[]>;
  public requestsAreTaken$!: Observable<IRequestsAreTaken>;

  constructor(
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private translateService: TranslateService,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(getRequests());
    this.store.dispatch(getRequestsAreTaken());
  }

  ionViewDidEnter() {
    this.requests$ = this.store.select('requests');
    this.requestsAreTaken$ = this.store.select('requestsAreTaken');
  }

  async presentRequestModal() {
    const modal = await this.modalCtrl.create({
      component: AddRequestPageComponent,
    });

    return modal.present();
  }

  itemSelected(request: IRequest) {
    const translationKeys: string[] = [
      'ALERTS.REQUEST_NAME',
      'ALERTS.REQUEST_ADDRESS',
      'ALERTS.REQUEST_MESSAGE',
      'ALERTS.REQUEST_CONTACT',
      'ALERTS.REQUEST_DONT_KNOW',
      'ALERTS.REQUEST_OK',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: ITranslatedValues) => {
        const requestName = values['ALERTS.REQUEST_NAME'],
          requestAddress = values['ALERTS.REQUEST_ADDRESS'],
          requestMessage = values['ALERTS.REQUEST_MESSAGE'],
          requestContact = values['ALERTS.REQUEST_CONTACT'],
          requestDontKnow = values['ALERTS.REQUEST_DONT_KNOW'],
          requestOK = values['ALERTS.REQUEST_OK'];

        const alert = await this.alertCtrl.create({
          header: request.title,
          message: [
            requestName + (request.name || requestDontKnow),
            requestAddress + (request.address || requestDontKnow),
            requestMessage + (request.message || requestDontKnow),
            requestContact + (request.contact || requestDontKnow),
          ].join('<br/>'),
          buttons: [requestOK],
        } as AlertOptions);

        await alert.present();
      });
  }
}
