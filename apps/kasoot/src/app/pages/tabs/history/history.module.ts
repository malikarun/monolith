import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../components/shared.module';
import { HistoryPageComponent } from './history.page';

const routes: Routes = [{ path: '', component: HistoryPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [HistoryPageComponent],
})
export class HistoryPageModule {}
