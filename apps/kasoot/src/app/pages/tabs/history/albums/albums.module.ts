import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { AlbumsPageComponent } from './albums.page';

const routes: Routes = [{ path: '', component: AlbumsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [AlbumsPageComponent],
})
export class AlbumsPageModule {}
