import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getAlbums } from '../../../../store/actions/album.actions';
import { IAlbumPayload } from '../../../../store/models/album.model';
import { IWriterPayload } from '../../../../store/models/writer.model';
import { IAppState } from '../../../../store/state';

@Component({
  selector: 'kasoot-albums',
  templateUrl: './albums.page.html',
  styleUrls: ['./albums.page.scss'],
})
export class AlbumsPageComponent {
  public albums!: Observable<IAlbumPayload[]>;
  public writer!: Observable<IWriterPayload | undefined>;
  private writerKey!: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<IAppState>
  ) {}

  ionViewDidEnter() {
    this.writerKey = this.route.snapshot.paramMap.get('writerKey') as string;
    this.store.dispatch(getAlbums({ writerKey: this.writerKey }));
    this.albums = this.store.select('albums');
    this.writer = this.store
      .select('writers')
      .pipe(
        map(
          (snapshot) =>
            snapshot &&
            snapshot.find(
              (writer: IWriterPayload) => writer.key === this.writerKey
            )
        )
      );
  }

  navigateToSongs(albumKey: string) {
    this.router.navigateByUrl(
      `/tabs/history/writers/${this.writerKey}/albums/${albumKey}/songs`
    );
  }
}
