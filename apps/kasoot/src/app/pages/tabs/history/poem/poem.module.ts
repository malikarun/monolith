import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { PoemPageComponent } from './poem.page';

const routes: Routes = [{ path: '', component: PoemPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [PoemPage]
})
export class PoemPageModule {}
