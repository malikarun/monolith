import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IPoem } from '../../../../store/models/poem.model';

@Component({
  selector: 'kasoot-poem',
  templateUrl: './poem.page.html',
  styleUrls: ['./poem.page.scss'],
})
export class PoemPageComponent {
  @Input() poem!: {
    payload: IPoem;
  };

  constructor(private modalCtrl: ModalController) {}

  dismiss() {
    this.modalCtrl.dismiss();
  }
}
