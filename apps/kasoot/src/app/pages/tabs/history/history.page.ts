import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getWriters } from '../../../store/actions/writer.actions';
import { IWriterPayload } from '../../../store/models/writer.model';
import { IAppState } from '../../../store/state';
@Component({
  selector: 'kasoot-history',
  templateUrl: 'history.page.html',
  styleUrls: ['history.page.scss'],
})
export class HistoryPageComponent {
  public writers!: Observable<IWriterPayload[]>;

  constructor(private router: Router, private store: Store<IAppState>) {
    this.store.dispatch(getWriters());
  }

  ionViewDidEnter() {
    this.writers = this.store.select('writers');
  }

  navigate(writerKey: string, page: string) {
    this.router.navigateByUrl(`/tabs/history/writers/${writerKey}/${page}`);
  }
}
