import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getSongs } from '../../../../store/actions/song.actions';
import { IAlbumPayload } from '../../../../store/models/album.model';
import { ISongPayload } from '../../../../store/models/song.model';
import { IAppState } from '../../../../store/state';
import { SongPageComponent } from '../song/song.page';

@Component({
  selector: 'kasoot-songs',
  templateUrl: './songs.page.html',
  styleUrls: ['./songs.page.scss'],
})
export class SongsPageComponent {
  public album!: Observable<IAlbumPayload>;
  public songs!: Observable<ISongPayload[]>;
  private albumKey!: string;

  constructor(
    private modalCtrl: ModalController,
    private router: ActivatedRoute,
    private store: Store<IAppState>
  ) {}

  ionViewDidEnter() {
    this.albumKey = this.router.snapshot.paramMap.get('albumKey') as string;
    this.store.dispatch(getSongs({ albumKey: this.albumKey }));
    this.songs = this.store.select('songs');
    this.album = this.store
      .select('albums')
      .pipe(
        map(
          (snapshot) =>
            snapshot.find(
              (album: IAlbumPayload) => album.key === this.albumKey
            ) || ({} as IAlbumPayload)
        )
      );
  }

  async presentSongModal(song: ISongPayload) {
    const songModal = await this.modalCtrl.create({
      component: SongPageComponent,
      componentProps: { song },
    });

    await songModal.present();
  }
}
