import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { SongsPageComponent } from './songs.page';

const routes: Routes = [
  {
    path: '',
    component: SongsPageComponent,
  },
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [SongsPageComponent],
})
export class SongsPageModule {}
