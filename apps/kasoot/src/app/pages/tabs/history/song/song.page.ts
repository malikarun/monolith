import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ISong } from '../../../../store/models/song.model';

@Component({
  selector: 'kasoot-song',
  templateUrl: './song.page.html',
  styleUrls: ['./song.page.scss'],
})
export class SongPageComponent {
  @Input() song!: {
    payload: ISong;
  };

  constructor(public modalCtrl: ModalController) {}

  dismiss() {
    this.modalCtrl.dismiss();
  }
}
