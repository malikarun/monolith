import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { SongPageComponent } from './song.page';

const routes: Routes = [{ path: '', component: SongPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  // declarations: [SongPage]
})
export class SongPageModule {}
