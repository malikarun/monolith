import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getPoems } from '../../../../store/actions/poem.actions';
import { IPoemPayload } from '../../../../store/models/poem.model';
import { IWriterPayload } from '../../../../store/models/writer.model';
import { IAppState } from '../../../../store/state';
import { PoemPageComponent } from '../poem/poem.page';

@Component({
  selector: 'kasoot-poems',
  templateUrl: './poems.page.html',
  styleUrls: ['./poems.page.scss'],
})
export class PoemsPageComponent {
  public poems!: Observable<IPoemPayload[]>;
  public writer!: Observable<IWriterPayload | undefined>;
  private writerKey!: string;

  constructor(
    private modalCtrl: ModalController,
    private route: ActivatedRoute,
    private store: Store<IAppState>
  ) {}

  ionViewDidEnter() {
    this.writerKey = this.route.snapshot.paramMap.get('writerKey') as string;

    this.store.dispatch(getPoems({ writerKey: this.writerKey }));
    this.poems = this.store.select('poems');
    this.writer = this.store
      .select('writers')
      .pipe(
        map(
          (snapshot) =>
            snapshot &&
            snapshot.find(
              (writer: IWriterPayload) => writer.key === this.writerKey
            )
        )
      );
  }

  async presentPoemModal(poem: IPoemPayload) {
    const poemModal = await this.modalCtrl.create({
      component: PoemPageComponent,
      componentProps: { poem },
    });

    await poemModal.present();
  }
}
