import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../../components/shared.module';
import { PoemsPageComponent } from './poems.page';

const routes: Routes = [{ path: '', component: PoemsPageComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [PoemsPageComponent],
})
export class PoemsPageModule {}
