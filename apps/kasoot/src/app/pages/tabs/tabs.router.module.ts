import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';
import { TabsPageComponent } from './tabs.page';

const defaultRoute: Route = {
  path: '',
  redirectTo: '/tabs/radio',
  pathMatch: 'full',
};

const routes: Routes = [
  {
    path: '',
    component: TabsPageComponent,
    children: [
      {
        path: 'radio',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./radio/radio.module').then((m) => m.RadioPageModule),
          },
          {
            path: 'requests',
            loadChildren: () =>
              import('./radio/requests/requests.module').then(
                (m) => m.RequestsPageModule
              ),
          },
          {
            path: 'youtube',
            loadChildren: () =>
              import('./radio/youtube/youtube.module').then(
                (m) => m.YoutubePageModule
              ),
          },
        ],
      },
      {
        path: 'entertainment',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./entertainment/entertainment.module').then(
                (m) => m.EntertainmentPageModule
              ),
          },
          {
            path: 'blog',
            loadChildren: () =>
              import('./entertainment/blog/blog.module').then(
                (m) => m.BlogPageModule
              ),
          },
          {
            path: 'dictionary',
            loadChildren: () =>
              import('./entertainment/dictionary/dictionary.module').then(
                (m) => m.DictionaryPageModule
              ),
          },
          {
            path: 'jokes',
            loadChildren: () =>
              import('./entertainment/jokes/jokes.module').then(
                (m) => m.JokesPageModule
              ),
          },
          {
            path: 'partners',
            loadChildren: () =>
              import('./entertainment/partners/partners.module').then(
                (m) => m.PartnersPageModule
              ),
          },
          {
            path: 'team',
            loadChildren: () =>
              import('./entertainment/team/team.module').then(
                (m) => m.TeamPageModule
              ),
          },
          {
            path: 'programs',
            loadChildren: () =>
              import('./entertainment/programs/programs.module').then(
                (m) => m.ProgramsPageModule
              ),
          },
          {
            path: 'weather',
            loadChildren: () =>
              import('./entertainment/weather/weather.module').then(
                (m) => m.WeatherPageModule
              ),
          },
          {
            path: 'mandi',
            loadChildren: () =>
              import('./entertainment/mandi/mandi.module').then(
                (m) => m.MandiPageModule
              ),
          },
          {
            path: 'proverbs',
            loadChildren: () =>
              import('./entertainment/proverbs/proverbs.module').then(
                (m) => m.ProverbsPageModule
              ),
          },
        ],
      },
      {
        path: 'history',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./history/history.module').then(
                (m) => m.HistoryPageModule
              ),
          },
          {
            path: 'writers/:writerKey/albums',
            loadChildren: () =>
              import('./history/albums/albums.module').then(
                (m) => m.AlbumsPageModule
              ),
          },
          {
            path: 'writers/:writerKey/poems',
            loadChildren: () =>
              import('./history/poems/poems.module').then(
                (m) => m.PoemsPageModule
              ),
          },
          {
            path: 'writers/:writerKey/albums/:albumKey/songs',
            loadChildren: () =>
              import('./history/songs/songs.module').then(
                (m) => m.SongsPageModule
              ),
          },
        ],
      },
      {
        path: 'social',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./social/social.module').then((m) => m.SocialPageModule),
          },
        ],
      },
      {
        path: 'settings',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./settings/settings.module').then(
                (m) => m.SettingsPageModule
              ),
          },
        ],
      },
      defaultRoute,
    ],
  },
  defaultRoute,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
