export interface ITranslatedValues {
  [key: string]: string;
}
