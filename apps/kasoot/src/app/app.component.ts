import { Component } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar, Style } from '@capacitor/status-bar';
import { GlobalService } from './services/global.service';
import {
  LanguageService,
  translationLoaded$,
} from './services/language.service';
import { MusicService } from './services/music.service';
import { PlayerTypeService } from './services/player-type.service';

@Component({
  selector: 'kasoot-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  isNativeDevice: boolean;
  isLoaded!: boolean;

  constructor(
    private musicService: MusicService,
    private languageService: LanguageService,
    private playerTypeService: PlayerTypeService,
    private globalService: GlobalService
  ) {
    this.isNativeDevice = Capacitor.isNativePlatform();
    this.initializeApp();
  }

  private async initializeApp() {
    // try {
    //     await DeviceMotionEvent.requestPermission()
    // } catch (e) {
    //     // Handle error
    //     return
    // }

    // wait for translation
    translationLoaded$.subscribe((loaded) => {
      this.isLoaded = loaded;

      if (this.isNativeDevice) {
        SplashScreen.hide();
      }
    });

    // Start language service
    this.languageService.setTranslationLang();

    // Start Player type
    this.playerTypeService.setCurrentPlayerType();

    // Start Global Service
    this.globalService.init();

    // start music service
    this.musicService.getAudioSource();

    if (this.isNativeDevice) {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      await StatusBar.setStyle({ style: Style.Light });
      await StatusBar.setBackgroundColor({ color: '#ff7733' });
    }
  }

  // private async setAnalytics() {
  //     await this.googleAnalytics.startTrackerWithId('UA-79794814-1')
  //     this.googleAnalytics.trackView('RadioKasootApp')
  // }
}
