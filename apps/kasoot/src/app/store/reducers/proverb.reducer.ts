import { createReducer, on } from '@ngrx/store';
import { IProverbContainer } from '../../store/models/proverb.model';
import * as Actions from '../actions/proverb.actions';

const initialState: IProverbContainer = {} as IProverbContainer;

export default createReducer(
  initialState,
  on(Actions.getFirstProverbSuccess, (state, { payload }) => {
    const proverb = payload[1];

    return {
      ...state,
      currentProverbKey: proverb && proverb.key,
      firstProverbKey: proverb && proverb.key,
      nextProverbKey: proverb && proverb.prevKey,
      proverb: proverb && proverb.payload.val(),
    };
  }),
  on(Actions.getNextProverbSuccess, (state, { payload }) => {
    const proverb = payload[1];

    return {
      ...state,
      currentProverbKey: proverb && proverb.key,
      nextProverbKey: proverb && proverb.prevKey,
      previousProverbKey: state.currentProverbKey,
      proverb: proverb && proverb.payload.val(),
    };
  }),
  on(Actions.getPreviousProverbSuccess, (state, { payload }) => {
    const proverb = payload[0];
    const lastProverb = payload[1];
    const firstIsCurrent = state.firstProverbKey === proverb.key;

    return {
      ...state,
      currentProverbKey: proverb && proverb.key,
      nextProverbKey: state.currentProverbKey,
      previousProverbKey: firstIsCurrent ? '' : lastProverb?.key,
      proverb: proverb && proverb.payload.val(),
    };
  })
);
