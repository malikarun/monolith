import { createReducer, on } from '@ngrx/store';
import { ILanguage } from '../../store/models/language.model';
import * as Actions from '../actions/language.actions';

const initialState: ILanguage = {} as ILanguage;

export default createReducer(
  initialState,
  on(Actions.setLanguagesSuccess, (state, { payload }) => ({
    ...state,
    all: payload,
  })),
  on(Actions.setDefaultLanguageSuccess, (state, { lang }) => ({
    ...state,
    default: lang,
  })),
  on(Actions.setCurrentLanguageSuccess, (state, { lang }) => ({
    ...state,
    current: lang,
  }))
);
