import { createReducer, on } from '@ngrx/store';
import { IRequest } from '../../store/models/request.model';
import * as Actions from '../actions/request.actions';

const initialState: IRequest = {} as IRequest;

export default createReducer(
  initialState,
  on(Actions.getRequestsSuccess, (_, { payload }) => payload)
);
