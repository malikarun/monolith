import { createReducer, on } from '@ngrx/store';
import { IMandi } from '../../store/models/mandi.model';
import * as Actions from '../actions/mandi.actions';

const defaultState: IMandi = {} as IMandi;

export default createReducer(
  defaultState,
  on(Actions.getMandiPricesSuccess, (_, { payload }) => payload)
);
