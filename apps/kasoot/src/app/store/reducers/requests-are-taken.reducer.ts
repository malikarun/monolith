import { createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/requests-are-taken.actions';

const initialState = {
  isRequestTaken: false,
};

export default createReducer(
  initialState,
  on(Actions.getRequestsAreTakenSuccess, (state, { payload }) => ({
    ...state,
    ...payload,
  }))
);
