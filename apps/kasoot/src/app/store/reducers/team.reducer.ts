import { createReducer, on } from '@ngrx/store';
import { ITeam } from '../../store/models/team.model';
import * as Actions from '../actions/team.actions';

const teamInitialState: ITeam = [];

export default createReducer(
  teamInitialState,
  on(Actions.getTeamSuccess, (_, { payload }) => payload)
);
