import { createReducer, on } from '@ngrx/store';
import { IProgram } from '../../store/models/program.model';
import * as Actions from '../actions/program.actions';

const defaultState: IProgram[] = [] as IProgram[];

export default createReducer(
  defaultState,
  on(Actions.getProgramSuccess, (_, { payload }) => payload)
);
