import { createReducer, on } from '@ngrx/store';
import { IPlayerType } from '../../store/models/player-type.model';
import * as Actions from '../actions/player-type.actions';

const defaultState: IPlayerType = {} as IPlayerType;

export default createReducer(
  defaultState,
  on(Actions.setPlayerTypeSuccess, (state, { payload }) => ({
    ...state,
    all: payload,
  })),
  on(Actions.setDefaultPlayerType, (state, { playerType }) => ({
    ...state,
    default: playerType,
  })),
  on(Actions.setCurrentPlayerType, (state, { playerType }) => ({
    ...state,
    current: playerType,
  }))
);
