import { createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/loader.actions';

const defaultState = {};

export default createReducer(
  defaultState,
  on(Actions.showLoader, (state) => ({ ...state, isShowing: true })),
  on(Actions.hideLoader, (state) => ({ ...state, isShowing: false }))
);
