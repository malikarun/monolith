import { createReducer, on } from '@ngrx/store';
import { IPoemPayload } from '../../store/models/poem.model';
import * as Actions from '../actions/poem.actions';

const defaultState: IPoemPayload[] = [] as IPoemPayload[];

export default createReducer(
  defaultState,
  on(Actions.getPoemsSuccess, (_, { payload }) => payload)
);
