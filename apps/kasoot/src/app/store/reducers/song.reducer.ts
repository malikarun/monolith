import { createReducer, on } from '@ngrx/store';
import { ISongPayload } from '../../store/models/song.model';
import * as Actions from '../actions/song.actions';

const songInitialState: ISongPayload[] = [];

export default createReducer(
  songInitialState,
  on(Actions.getSongsSuccess, (_, { payload }) => payload)
);
