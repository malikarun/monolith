import { createReducer, on } from '@ngrx/store';
import { ITranslation } from '../../store/models/translation.model';
import * as Actions from '../actions/translation.actions';

const translationInitialState: ITranslation = {
  en: {},
  hi: {},
  ur: {},
};

export default createReducer(
  translationInitialState,
  on(Actions.getTranslationsSuccess, (_, { payload }) => payload)
);
