import { createReducer, on } from '@ngrx/store';
import { find, flatten, get } from 'lodash';
import { IPlayer } from '../../store/models/player.model';
import * as Actions from '../actions/player.actions';

const defaultState: IPlayer = {} as IPlayer;

export default createReducer(
  defaultState,
  on(Actions.getServerSuccess, (state, { payload }) => {
    const server = payload;
    const streams = [
      { label: 'Live Radio', url: server.liveUrl },
      { label: 'Raagni', url: server.raagniUrl },
      { label: 'Gaane', url: server.songsUrl },
    ];
    const currentStream = streams[0];

    return {
      ...state,
      server,
      streams,
      currentStream,
      audioSrc: currentStream.url,
    };
  }),
  on(Actions.getSongTitleSuccess, (state, { payload }) => {
    // Make an array and flatten
    // To handle single or multiple streams
    const currentSrc = find(flatten([get(payload, 'icestats.source')]), [
      'server_url',
      state?.currentStream?.url,
    ]);

    return {
      ...state,
      songTitle: currentSrc ? currentSrc.title : 'गाणा का नाम कोनी बेरा',
    };
  }),
  on(Actions.updateIsPlayingSuccess, (state, { isPlaying }) => ({
    ...state,
    isPlaying,
  })),
  on(Actions.updateStreamSuccess, (state, { payload }) => ({
    ...state,
    currentStream: payload,
    audioSrc: payload.url,
  }))
);
