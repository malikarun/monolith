import { createReducer, on } from '@ngrx/store';
import { IJokeContainer } from '../../store/models/joke.model';
import * as Actions from '../actions/joke.actions';

const initialState: IJokeContainer = {} as IJokeContainer;

export default createReducer(
  initialState,
  on(Actions.getFirstJokeSuccess, (state, { payload }) => {
    const joke = payload[1];

    return {
      ...state,
      firstJokeKey: joke && joke.key,
      currentJokeKey: joke && joke.key,
      nextJokeKey: joke && joke.prevKey,
      joke: joke && joke.payload.val(),
    };
  }),
  on(Actions.getNextJokeSuccess, (state, { payload }) => {
    const joke = payload[1];

    return {
      ...state,
      currentJokeKey: joke && joke.key,
      nextJokeKey: joke && joke.prevKey,
      previousJokeKey: joke && state.currentJokeKey,
      joke: joke && joke.payload.val(),
    };
  }),
  on(Actions.getPreviousJokeSuccess, (state, { payload }) => {
    const joke = payload[0];
    const lastJoke = payload[1];
    const firstIsCurrent = state.firstJokeKey === joke.key;

    return {
      ...state,
      currentJokeKey: joke && joke.key,
      nextJokeKey: state.currentJokeKey,
      previousJokeKey: firstIsCurrent ? '' : lastJoke?.key,
      joke: joke && joke.payload.val(),
    };
  })
);
