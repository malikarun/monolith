import { createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/weather.actions';

export default createReducer(
  {},
  on(Actions.getWeatherSuccess, (state, payload) => ({
    ...state,
    data: payload,
    info: undefined,
  })),
  on(Actions.getWeatherDescriptionSuccess, (state, payload) => ({
    ...state,
    info: payload,
  }))
);
