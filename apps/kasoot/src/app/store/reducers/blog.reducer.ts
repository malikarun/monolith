import { createReducer, on } from '@ngrx/store';
import { IBlogPost } from '../../store/models/blog.model';
import * as Actions from '../actions/blog.actions';

const initialState: IBlogPost = {} as IBlogPost;

export default createReducer(
  initialState,
  on(Actions.getBlogPostSuccess, (_, { payload }) => payload)
);
