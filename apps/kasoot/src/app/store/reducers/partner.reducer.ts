import { createReducer, on } from '@ngrx/store';
import { IPartner } from '../../store/models/partner.model';
import * as Actions from '../actions/partner.actions';

const defaultState: IPartner[] = [];

export default createReducer(
  defaultState,
  on(Actions.getPartnersSuccess, (_, { payload }) => payload)
);
