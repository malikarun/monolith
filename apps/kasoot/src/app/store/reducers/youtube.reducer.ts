import { createReducer, on } from '@ngrx/store';
import { IYoutubePost } from '../../store/models/youtube.model';
import * as Actions from '../actions/youtube.actions';

const youTubeInitalState: {
  items: IYoutubePost[];
} = { items: [] };

export default createReducer(
  youTubeInitalState,
  on(Actions.getYoutubeSuccess, (state, { payload }) => ({
    nextPageToken: payload.nextPageToken,
    items: [...(state ? state?.items : []), ...payload.items],
  }))
);
