import { createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/writer.actions';
import { IWriter } from '../models/writer.model';

const initialState: IWriter[] = [];

export default createReducer(
  initialState,
  on(Actions.getWritersSuccess, (_, { writers }) => writers)
);
