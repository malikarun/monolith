import { createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/album.actions';
import { IAlbumPayload } from '../models/album.model';

const albumDefaultState: IAlbumPayload[] = [];

export default createReducer(
  albumDefaultState,
  on(Actions.getAlbumsSuccess, (_, { payload }) => payload)
);
