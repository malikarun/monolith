export interface IJoke {
  username: string;
  title: string;
  approved: boolean;
}

export interface IJokeContainer {
  currentJokeKey: string;
  firstJokeKey: string;
  joke: IJoke;
  nextJokeKey: string;
  previousJokeKey: string;
}
