export interface IStream {
  label: string;
  url: string;
}

export interface IServer {
  liveUrl: string;
  newsWebsiteUrl: string;
  raagniUrl: string;
  songsUrl: string;
  statsUrl: string;
  websiteUrl: string;
  openWeatherMapIconUrl: string;
  logoUrl: string;
}

export interface IPlayer {
  isPlaying: boolean;
  songTitle: string;
  currentTime: number;
  currentStream: IStream;
  audioSrc: string;
  server: IServer;
  streams: IStream[];
}

export interface IIceSource {
  server_url: string;
}

export interface IIcestats {
  source: IIceSource;
}

export interface IIcecastData {
  icestats: IIcestats;
}
