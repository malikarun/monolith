export interface IWord {
  username: string;
  approved: boolean;
  name: string;
  hindi: string;
  english: string;
  similar: string;
  example: string;
}

export interface IDictionary {
  currentWordKey: string;
  firstWordKey: string;
  word: IWord;
  nextWordKey: string;
  previousWordKey: string;
}
