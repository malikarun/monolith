export interface IRequest {
  title: string;
  name: string;
  address: string;
  message: string;
  contact: string;
}
