export interface IWeatherData {
  id: number;
  description: string;
  main: string;
  icon: string;
}

export interface IWind {
  deg: number;
  speed: number;
}

export interface IMainWeatherData {
  humidity: number;
  pressure: number;
  temp: number;
  temp_max: number;
  temp_min: number;
}

export interface ICoord {
  lat: number;
  lon: number;
}

export interface IClouds {
  all: number;
}

export interface IWeather {
  weather: IWeatherData[];
  wind: IWind;
  main: IMainWeatherData;
  coord: ICoord;
  clouds: IClouds;
}

export interface IWeatherDescription {
  title: string;
  description: string;
}

export interface IWeatherContainer {
  data: IWeather;
  info: IWeatherDescription;
}

export interface GeopositionPayload {
  latitude: number;
  longitude: number;
}
