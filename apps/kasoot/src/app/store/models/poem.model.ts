export interface IPoem {
  title: string;
  content: string;
}

export interface IPoemPayload {
  payload: IPoem;
  key: string;
}
