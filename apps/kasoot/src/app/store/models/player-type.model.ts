export interface IPlayerType {
  label: string;
  value: string;
}

export interface IPlayerTypeContainer {
  all: IPlayerType[];
  default: IPlayerType;
  current: IPlayerType;
}
