export interface IProgram {
  title: string;
  description: string;
  time: string;
}
