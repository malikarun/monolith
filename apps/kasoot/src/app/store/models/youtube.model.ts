export interface IYoutubePostSnippet {
  title: string;
  thumbnails: {
    high: {
      url: string;
    };
  };
}
export interface IYoutubePost {
  snippet: IYoutubePostSnippet;
  id: {
    videoId: string;
  };
}

export interface IYoutube {
  nextPageToken: string;
  items: IYoutubePost[];
}
