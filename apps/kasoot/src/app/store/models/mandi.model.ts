export interface IMandi {
  title: string;
  desc: string;
  org_type: string;
  org?: string[] | null;
  sector?: string[] | null;
  source: string;
  field?: IField[] | null;
  total: number;
  count: number;
  limit: string;
  offset: string;
  records?: IRecord[] | null;
}

export interface IField {
  id: string;
  name: string;
  type: string;
}

export interface IRecord {
  timestamp: string;
  state: string;
  district: string;
  market: string;
  commodity: string;
  variety: string;
  arrival_date: string;
  min_price: string;
  max_price: string;
  modal_price: string;
}
