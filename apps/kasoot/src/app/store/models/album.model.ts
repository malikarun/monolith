export interface IAlbum {
  title: string;
  key: string;
}

export interface IAlbumPayload {
  payload: IAlbum;
  key: string;
}
