export interface ILanguage {
  label: string;
  value: string;
}

export interface ILanguageContainer {
  all: ILanguage[];
  current: ILanguage;
  default: ILanguage;
}
