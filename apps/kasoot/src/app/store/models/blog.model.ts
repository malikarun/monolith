export interface IBlogPost {
  title: string;
  content: string;
}

export interface IBlog {
  items: IBlogPost[];
  nextPageToken: string;
}
