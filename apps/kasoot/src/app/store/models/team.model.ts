export interface ITeamMember {
  photo_url: string;
  name: string;
  title: string;
}

export type ITeam = ITeamMember[];
