export interface IProverb {
  approved: boolean;
  title: string;
  username: string;
  meaning: string;
}

export interface IProverbContainer {
  currentProverbKey: string;
  firstProverbKey: string;
  proverb: IProverb;
  nextProverbKey: string;
  previousProverbKey: string;
}
