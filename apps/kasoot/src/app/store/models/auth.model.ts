export interface IProvider {
  uid: string;
}

export interface IAuth {
  avatarID: string;
  avatarUrl: string;
  displayName: string;
  loggedIn: boolean;
  providerData: IProvider[];
}
