export interface IWriter {
  name: string;
  key: string;
}

export interface IWriterPayload {
  payload: IWriter;
  key: string;
}
