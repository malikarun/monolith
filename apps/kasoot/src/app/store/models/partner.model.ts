export interface IPartner {
  name: string;
  photo_url: string;
  websiteUrl: string;
}
