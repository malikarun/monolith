export interface ISong {
  title: string;
  content: string;
}

export interface ISongPayload {
  payload: ISong;
  key: string;
}
