export interface IRequestsAreTaken {
  trueOrFalse: boolean;
  onMessage: string;
  offMessage: string;
  offContent: string;
}
