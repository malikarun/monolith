export interface ITranslationValues {
  [key: string]: string | ITranslationValues;
}

export interface ITranslation {
  hi: ITranslationValues;
  en: ITranslationValues;
  ur: ITranslationValues;
}
