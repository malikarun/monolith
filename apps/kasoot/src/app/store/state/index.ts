import { IAlbumPayload } from '../models/album.model';
import { IAuth } from '../models/auth.model';
import { IBlog } from '../models/blog.model';
import { IDictionary } from '../models/dictionary.model';
import { IJokeContainer } from '../models/joke.model';
import { ILanguageContainer } from '../models/language.model';
import { ILoader } from '../models/loader.model';
import { IMandi } from '../models/mandi.model';
import { IPartner } from '../models/partner.model';
import { IPlayerTypeContainer } from '../models/player-type.model';
import { IPlayer } from '../models/player.model';
import { IPoemPayload } from '../models/poem.model';
import { IProgram } from '../models/program.model';
import { IProverbContainer } from '../models/proverb.model';
import { IRequest } from '../models/request.model';
import { IRequestsAreTaken } from '../models/requests-are-taken.model';
import { ISongPayload } from '../models/song.model';
import { ITeam } from '../models/team.model';
import { ITranslation } from '../models/translation.model';
import { IWeatherContainer } from '../models/weather.model';
import { IWriterPayload } from '../models/writer.model';
import { IYoutube } from '../models/youtube.model';

export interface IAppState {
  albums: IAlbumPayload[];
  auth: IAuth;
  blog: IBlog;
  dictionary: IDictionary;
  jokes: IJokeContainer;
  languages: ILanguageContainer;
  loader: ILoader;
  mandi: IMandi;
  partners: IPartner[];
  playerTypes: IPlayerTypeContainer;
  player: IPlayer;
  poems: IPoemPayload[];
  programs: IProgram[];
  proverbs: IProverbContainer;
  requests: IRequest[];
  requestsAreTaken: IRequestsAreTaken;
  songs: ISongPayload[];
  team: ITeam;
  translations: ITranslation;
  weather: IWeatherContainer;
  writers: IWriterPayload[];
  youtube: IYoutube;
}

export interface ISnapshotPayload {
  val: () => any;
}

export interface ISnapshot {
  key: string;
  prevKey: string;
  payload: ISnapshotPayload;
}
