import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { getSongs, getSongsSuccess } from '../actions/song.actions';

@Injectable()
export class SongEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getSongs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getSongs),
      switchMap(({ albumKey }) =>
        this.database
          .list('/songs', (ref) =>
            ref.orderByChild('albumKey').equalTo(albumKey)
          )
          .snapshotChanges()
          .pipe(
            map((snapshot) =>
              snapshot.map(({ key, payload }) => ({
                key: key,
                payload: payload.val(),
              }))
            )
          )
      ),
      map((payload: any) => getSongsSuccess({ payload }))
    )
  );
}
