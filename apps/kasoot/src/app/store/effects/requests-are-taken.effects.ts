import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import {
  getRequestsAreTaken,
  getRequestsAreTakenSuccess,
} from '../actions/requests-are-taken.actions';

@Injectable()
export class RequestsAreTakenEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getRequestsAreTaken),
      switchMap(() => this.database.object('requestsAreTaken').valueChanges()),
      map((payload: any) => getRequestsAreTakenSuccess({ payload }))
    )
  );
}
