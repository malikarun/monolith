import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { getAlbums, getAlbumsSuccess } from '../actions/album.actions';
import {
  login,
  loginSuccess,
  logout,
  logoutSuccess,
} from '../actions/auth.actions';
import { getBlogPost, getBlogPostSuccess } from '../actions/blog.actions';
import {
  createWord,
  createWordSuccess,
  getFirstWord,
  getFirstWordSuccess,
  getPreviousWord,
} from '../actions/dictionary.actions';
import {
  createJoke,
  createJokeSuccess,
  getFirstJoke,
  getFirstJokeSuccess,
  getNextJoke,
  getNextJokeSuccess,
  getPreviousJoke,
  getPreviousJokeSuccess,
} from '../actions/joke.actions';
import {
  setDefaultLanguage,
  setDefaultLanguageSuccess,
  setLanguages,
  setLanguagesSuccess,
} from '../actions/language.actions';
import { hideLoader, showLoader } from '../actions/loader.actions';
import { getPartners, getPartnersSuccess } from '../actions/partner.actions';
import { getPoems, getPoemsSuccess } from '../actions/poem.actions';
import { getProgram, getProgramSuccess } from '../actions/program.actions';
import { getRequests, getRequestsSuccess } from '../actions/request.actions';
import {
  getRequestsAreTaken,
  getRequestsAreTakenSuccess,
} from '../actions/requests-are-taken.actions';
import { getSongs, getSongsSuccess } from '../actions/song.actions';
import { getTeam, getTeamSuccess } from '../actions/team.actions';
import {
  getTranslations,
  getTranslationsSuccess,
} from '../actions/translation.actions';
import { getWeather, getWeatherSuccess } from '../actions/weather.actions';
import { getWriters, getWritersSuccess } from '../actions/writer.actions';
import { getYoutube, getYoutubeSuccess } from '../actions/youtube.actions';

const startLoadingActions = [
  getAlbums,
  login,
  logout,
  getBlogPost,
  getFirstWord,
  createJoke,
  getNextJoke,
  getFirstJoke,
  getPreviousJoke,
  getPreviousWord,
  createWord,
  setLanguages,
  setDefaultLanguage,
  getPartners,
  getPoems,
  getProgram,
  getRequests,
  getRequestsAreTaken,
  getSongs,
  getTeam,
  getTranslations,
  getWeather,
  getWriters,
  getYoutube,
];

const stopLoadingActions = [
  getAlbumsSuccess,
  loginSuccess,
  logoutSuccess,
  getBlogPostSuccess,
  createWordSuccess,
  getFirstWordSuccess,
  getPreviousJokeSuccess,
  getFirstJokeSuccess,
  getNextJokeSuccess,
  getPreviousJokeSuccess,
  createJokeSuccess,
  setLanguagesSuccess,
  setDefaultLanguageSuccess,
  getPartnersSuccess,
  getPoemsSuccess,
  getProgramSuccess,
  getRequestsSuccess,
  getRequestsAreTakenSuccess,
  getSongsSuccess,
  getTeamSuccess,
  getTranslationsSuccess,
  getWeatherSuccess,
  getWritersSuccess,
  getYoutubeSuccess,
];

@Injectable()
export class LoaderEffects {
  constructor(private actions$: Actions) {}

  showLoader$ = createEffect(() =>
    this.actions$.pipe(ofType(...startLoadingActions), map(showLoader))
  );

  hideLoader$ = createEffect(() =>
    this.actions$.pipe(ofType(...stopLoadingActions), map(hideLoader))
  );
}
