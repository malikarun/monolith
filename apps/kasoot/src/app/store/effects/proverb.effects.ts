import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createProverb,
  createProverbSuccess,
  getFirstProverb,
  getFirstProverbSuccess,
  getNextProverb,
  getNextProverbSuccess,
  getPreviousProverb,
  getPreviousProverbSuccess,
} from '../actions/proverb.actions';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class ProverbEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getFirstProverb$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFirstProverb),
      switchMap(() =>
        this.database
          .list('proverbs', (ref) =>
            ref.orderByChild('approved').equalTo(true).limitToLast(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getFirstProverbSuccess({ payload }))
    )
  );

  getNextProverb$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getNextProverb),
      switchMap(({ key }) =>
        this.database
          .list('proverbs', (ref) => ref.orderByKey().endAt(key).limitToLast(2))
          .snapshotChanges()
      ),
      map((payload: any) => getNextProverbSuccess({ payload }))
    )
  );

  getPreviousProverb$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPreviousProverb),
      switchMap(({ key }) =>
        this.database
          .list('proverbs', (ref) =>
            ref.orderByKey().startAt(key).limitToFirst(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getPreviousProverbSuccess({ payload }))
    )
  );

  createProverb$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createProverb),
      switchMap(({ payload }) => this.database.list('proverbs').push(payload)),
      map(() => createProverbSuccess())
    )
  );
}
