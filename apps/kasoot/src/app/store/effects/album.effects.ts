import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { getAlbums, getAlbumsSuccess } from '../actions/album.actions';
import { IAlbumPayload } from '../models/album.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class AlbumEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getAlbums$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getAlbums),
      switchMap(({ writerKey }) =>
        this.database
          .list('/albums', (ref) =>
            ref.orderByChild('writerKey').equalTo(writerKey)
          )
          .snapshotChanges()
          .pipe(
            map((snapshot) =>
              snapshot.map(({ key, payload }) => ({
                key: key,
                payload: payload.val(),
              }))
            )
          )
      ),
      map((payload: any) => getAlbumsSuccess({ payload }))
    )
  );
}
