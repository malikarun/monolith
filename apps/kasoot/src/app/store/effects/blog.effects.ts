import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { BlogService } from '../../services/blog.service';
import { getBlogPost, getBlogPostSuccess } from '../actions/blog.actions';

@Injectable()
export class BlogEffects {
  constructor(private actions$: Actions, private blogService: BlogService) {}

  getBlogPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getBlogPost),
      switchMap(({ token }) => this.blogService.getNextPost(token)),
      map((payload: any) => getBlogPostSuccess({ payload }))
    )
  );
}
