import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  setCurrentPlayerType,
  setCurrentPlayerTypeSuccess,
  setDefaultPlayerType,
  setDefaultPlayerTypeSuccess,
  setPlayerType,
  setPlayerTypeSuccess,
} from '../actions/player-type.actions';
import { map } from 'rxjs/operators';

@Injectable()
export class PlayerTypeEffects {
  constructor(private actions$: Actions) {}

  setPlayerTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setPlayerType),
      map(({ payload }) => setPlayerTypeSuccess({ payload }))
    )
  );

  setDefaultPlayerType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setDefaultPlayerType),
      map(({ playerType }) => setDefaultPlayerTypeSuccess({ playerType }))
    )
  );

  setCurrentPlayerType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setCurrentPlayerType),
      map(({ playerType }) => setCurrentPlayerTypeSuccess({ playerType }))
    )
  );
}
