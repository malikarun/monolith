import { Injectable } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireList,
} from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createRequest,
  getRequests,
  getRequestsSuccess,
} from '../actions/request.actions';
import { IRequest } from '../models/request.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class RequestEffects {
  private requestRef: AngularFireList<IRequest>;

  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {
    this.requestRef = this.database.list('requests');
  }

  getRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getRequests),
      switchMap(() => this.requestRef.valueChanges()),
      map((payload: any) => getRequestsSuccess({ payload }))
    )
  );

  createRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createRequest),
      switchMap(({ payload }) => this.requestRef.push(payload)),
      map(() => getRequests())
    )
  );
}
