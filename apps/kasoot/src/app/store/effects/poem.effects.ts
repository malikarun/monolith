import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { getPoems, getPoemsSuccess } from '../actions/poem.actions';
import { IPoemPayload } from '../models/poem.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class PoemEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getPoems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPoems),
      switchMap(({ writerKey }) =>
        this.database
          .list('/poems', (ref) =>
            ref.orderByChild('writerKey').equalTo(writerKey)
          )
          .snapshotChanges()
          .pipe(
            map((snapshot) =>
              snapshot.map(({ key, payload }) => ({
                key: key,
                payload: payload.val(),
              }))
            )
          )
      ),
      map((payload: any) => getPoemsSuccess({ payload }))
    )
  );
}
