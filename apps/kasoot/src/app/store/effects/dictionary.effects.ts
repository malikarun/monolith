import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import {
  createWord,
  createWordSuccess,
  getFirstWord,
  getFirstWordSuccess,
  getNextWord,
  getNextWordSuccess,
  getPreviousWord,
  getPreviousWordSuccess,
} from '../actions/dictionary.actions';

@Injectable()
export class DictionaryEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getFirstWord$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFirstWord),
      switchMap(() =>
        this.database
          .list('dictionary', (ref) =>
            ref.orderByChild('approved').equalTo(true).limitToLast(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getFirstWordSuccess({ payload }))
    )
  );

  getNextWord$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getNextWord),
      switchMap(({ key }) =>
        this.database
          .list('dictionary', (ref) =>
            ref.orderByKey().endAt(key).limitToLast(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getNextWordSuccess({ payload }))
    )
  );

  getPreviousWord$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPreviousWord),
      switchMap(({ key }) =>
        this.database
          .list('dictionary', (ref) =>
            ref.orderByKey().startAt(key).limitToFirst(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getPreviousWordSuccess({ payload }))
    )
  );

  createWord$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createWord),
      switchMap(({ payload }) =>
        this.database.list('dictionary').push(payload)
      ),
      map(() => createWordSuccess())
    )
  );
}
