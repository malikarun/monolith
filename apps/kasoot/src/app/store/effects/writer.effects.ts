import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { getWriters, getWritersSuccess } from '../actions/writer.actions';
import { IWriterPayload } from '../models/writer.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class WriterEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getWriters$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getWriters),
      switchMap(() =>
        this.database
          .list('writers')
          .snapshotChanges()
          .pipe(
            map((snapshot) =>
              snapshot.map(({ key, payload }) => ({
                key: key,
                payload: payload.val(),
              }))
            )
          )
      ),
      map((writers: any) => getWritersSuccess({ writers }))
    )
  );
}
