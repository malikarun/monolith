import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { getTeam, getTeamSuccess } from '../actions/team.actions';

@Injectable()
export class TeamEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getTeam$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTeam),
      switchMap(() => this.database.list('team').valueChanges()),
      map((payload: any) => getTeamSuccess({ payload }))
    )
  );
}
