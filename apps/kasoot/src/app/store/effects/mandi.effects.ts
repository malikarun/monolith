import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { MandiService } from '../../services/mandi.service';
import {
  getMandiPrices,
  getMandiPricesSuccess,
} from '../actions/mandi.actions';

@Injectable()
export class MandiEffects {
  constructor(private actions$: Actions, private mandiService: MandiService) {}

  getMandiPrice$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getMandiPrices),
      switchMap(() => this.mandiService.getMandiPrices()),
      map((payload: any) => getMandiPricesSuccess({ payload }))
    )
  );
}
