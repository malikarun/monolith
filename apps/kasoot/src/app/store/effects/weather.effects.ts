import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WeatherService } from '../../services/weather.service';
import {
  getWeather,
  getWeatherDescription,
  getWeatherDescriptionSuccess,
  getWeatherSuccess,
} from '../actions/weather.actions';
import { IWeather, IWeatherDescription } from '../models/weather.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class WeatherEffects {
  constructor(
    private actions$: Actions,
    private weatherService: WeatherService,
    private db: AngularFireDatabase
  ) {}

  getWeather$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getWeather),
      switchMap(({ latitude, longitude }) =>
        this.weatherService.getCurrentWeather(latitude, longitude)
      ),
      map((payload: any) => getWeatherSuccess(payload))
    )
  );

  getWeatherDescription$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getWeatherDescription),
      switchMap(({ code }) => this.db.object(`weather/${code}`).valueChanges()),
      map((payload: any) => getWeatherDescriptionSuccess(payload))
    )
  );
}
