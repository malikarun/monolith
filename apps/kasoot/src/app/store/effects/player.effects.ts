import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  getServer,
  getServerSuccess,
  getSongTitle,
  getSongTitleSuccess,
  updateIsPlaying,
  updateIsPlayingSuccess,
  updateStream,
  updateStreamSuccess,
} from '../actions/player.actions';
import { IIcecastData, IServer } from '../models/player.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class PlayerEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase,
    private http: HttpClient
  ) {}

  getServer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getServer),
      switchMap(() => this.database.object('server').valueChanges()),
      map((payload: any) => getServerSuccess({ payload }))
    )
  );

  getSongTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getSongTitle),
      switchMap(({ statsUrl }) => this.http.get(statsUrl)),
      map((payload: any) => getSongTitleSuccess({ payload }))
    )
  );

  updatePlaying$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateIsPlaying),
      map(({ isPlaying }) => updateIsPlayingSuccess({ isPlaying }))
    )
  );

  updateStream$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateStream),
      map(({ payload }) => updateStreamSuccess({ payload }))
    )
  );
}
