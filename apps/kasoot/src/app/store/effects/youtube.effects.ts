import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { YoutubeService } from '../../services/youtube.service';
import { getYoutube, getYoutubeSuccess } from '../actions/youtube.actions';

@Injectable()
export class YoutubeEffects {
  constructor(
    private actions$: Actions,
    private youtubeService: YoutubeService
  ) {}

  getYoutube$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getYoutube),
      switchMap(({ payload }) => this.youtubeService.getPosts(payload)),
      map((payload: any) => getYoutubeSuccess({ payload }))
    )
  );
}
