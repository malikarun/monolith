import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import {
  setCurrentLanguage,
  setCurrentLanguageSuccess,
  setDefaultLanguage,
  setDefaultLanguageSuccess,
  setLanguages,
  setLanguagesSuccess,
} from '../actions/language.actions';

@Injectable()
export class LanguageEffects {
  constructor(private actions$: Actions) {}

  setLanguages$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setLanguages),
      map(({ payload }) => setLanguagesSuccess({ payload }))
    )
  );

  setDefaultLanguage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setDefaultLanguage),
      map(({ lang }) => setDefaultLanguageSuccess({ lang }))
    )
  );

  setCurrentLanguage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setCurrentLanguage),
      map(({ lang }) => setCurrentLanguageSuccess({ lang }))
    )
  );
}
