import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import {
  createJoke,
  createJokeSuccess,
  getFirstJoke,
  getFirstJokeSuccess,
  getNextJoke,
  getNextJokeSuccess,
  getPreviousJoke,
  getPreviousJokeSuccess,
} from '../actions/joke.actions';
import { ISnapshot } from '../state';

@Injectable()
export class JokeEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getFirstJoke$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFirstJoke),
      switchMap(() =>
        this.database
          .list('jokes', (ref) =>
            ref.orderByChild('approved').equalTo(true).limitToLast(2)
          )
          .snapshotChanges()
      ),
      map((payload: any) => getFirstJokeSuccess({ payload }))
    )
  );

  getNextJoke$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getNextJoke),
      switchMap(({ key }) =>
        this.database
          .list('jokes', (ref) => ref.orderByKey().endAt(key).limitToLast(2))
          .snapshotChanges()
      ),
      map((payload: any) => getNextJokeSuccess({ payload }))
    )
  );

  getPreviousJoke$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPreviousJoke),
      switchMap(({ key }) =>
        this.database
          .list('jokes', (ref) => ref.orderByKey().startAt(key).limitToFirst(2))
          .snapshotChanges()
      ),
      map((payload: any) => getPreviousJokeSuccess({ payload }))
    )
  );

  createJoke$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createJoke),
      switchMap(({ payload }) => this.database.list('jokes').push(payload)),
      map(() => createJokeSuccess())
    )
  );
}
