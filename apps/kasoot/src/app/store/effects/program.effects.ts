import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { getProgram, getProgramSuccess } from '../actions/program.actions';
import { IProgram } from '../models/program.model';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class ProgramEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getPrograms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getProgram),
      switchMap(() => this.database.list('programs').valueChanges()),
      map((payload: any) => getProgramSuccess({ payload }))
    )
  );
}
