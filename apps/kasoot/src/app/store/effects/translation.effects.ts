import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import {
  getTranslations,
  getTranslationsSuccess,
} from '../actions/translation.actions';

@Injectable()
export class TranslationEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getTranslations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTranslations),
      mergeMap(() => this.database.object('translations').valueChanges()),
      map((payload: any) => getTranslationsSuccess({ payload }))
    )
  );
}
