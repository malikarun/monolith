import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { getPartners, getPartnersSuccess } from '../actions/partner.actions';

@Injectable()
export class PartnerEffects {
  constructor(
    private actions$: Actions,
    private database: AngularFireDatabase
  ) {}

  getPartners$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPartners),
      switchMap(() => this.database.list('partners').valueChanges()),
      map((payload: any) => getPartnersSuccess({ payload }))
    )
  );
}
