import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class ShakeService {
  public shakeMessages!: Record<string, string>[];
  public shakeCount = -1;
  public shakeAlert!: HTMLIonAlertElement;

  constructor(
    private alertCtrl: AlertController,
    private translateService: TranslateService
  ) {}

  // populate shake messages
  populateShakeMessages() {
    const translationKeys: Array<string> = [
      'ALERTS.SHAKE_MESSAGE_ONE_TITLE',
      'ALERTS.SHAKE_MESSAGE_ONE_MESSAGE',
      'ALERTS.SHAKE_MESSAGE_TWO_TITLE',
      'ALERTS.SHAKE_MESSAGE_TWO_MESSAGE',
      'ALERTS.SHAKE_MESSAGE_THREE_TITLE',
      'ALERTS.SHAKE_MESSAGE_THREE_MESSAGE',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe(async (values: Record<string, string>) => {
        const oneTitle: string = values['ALERTS.SHAKE_MESSAGE_ONE_TITLE'],
          oneMessage: string = values['ALERTS.SHAKE_MESSAGE_ONE_MESSAGE'],
          twoTitle: string = values['ALERTS.SHAKE_MESSAGE_TWO_TITLE'],
          twoMessage: string = values['ALERTS.SHAKE_MESSAGE_TWO_MESSAGE'],
          threeTitle: string = values['ALERTS.SHAKE_MESSAGE_THREE_TITLE'],
          threeMessage: string = values['ALERTS.SHAKE_MESSAGE_THREE_MESSAGE'];

        this.shakeMessages = [
          { title: oneTitle, message: oneMessage },
          { title: twoTitle, message: twoMessage },
          { title: threeTitle, message: threeMessage },
        ];
      });
  }

  getShakeCount(): number {
    this.shakeCount++;
    if (this.shakeCount >= this.shakeMessages.length) {
      this.shakeCount = 0;
    }

    return this.shakeCount;
  }

  // watch shake gesture
  watchShake() {
    // Motion.addListener('accel', async (event) => {
    //     console.log('Device motion event:', event)
    //     // this.populateShakeMessages()
    //     // let okMessage: string
    //     // this.translateService
    //     //     .get('ALERTS.SHAKE_MESSAGE_OK_BUTTON')
    //     //     .subscribe((value) => (okMessage = value))
    //     // if (!!this.shakeAlert) {
    //     //     this.shakeAlert.dismiss()
    //     // }
    //     // const message = this.shakeMessages[this.getShakeCount()]
    //     // this.shakeAlert = await this.alertCtrl.create({
    //     //     header: message['title'],
    //     //     message: message['message'],
    //     //     buttons: [okMessage],
    //     // })
    //     // await this.shakeAlert.present()
    // })
  }
}
