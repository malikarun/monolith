import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppLauncher } from '@capacitor/app-launcher';
import { Browser } from '@capacitor/browser';
import { Capacitor } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';
import { ShakeService } from '../services/shake.service';
import { LoadingService } from './loading.service';
import { NetworkService } from './network.service';
import { NotificationService } from './notification.service';
import { RatingService } from './rating.service';

@Injectable({ providedIn: 'root' })
export class GlobalService {
  public logoImageUrl =
    'https://graph.facebook.com/331985113591743/picture?type=large';
  private isNativeDevice: boolean;

  constructor(
    private title: Title,
    private shakeService: ShakeService,
    private networkService: NetworkService,
    private ratingService: RatingService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private notificationService: NotificationService
  ) {
    this.isNativeDevice = Capacitor.isNativePlatform();
  }

  async init(): Promise<void> {
    // init networtk status
    this.networkService.init();

    // Init Loader
    this.loadingService.init();

    // set app rating

    if (this.isNativeDevice) {
      // Subscribe for notifications
      this.notificationService.subscribe();

      // Rate Service
      this.ratingService.setAppRating();

      // watch shake events
      this.shakeService.watchShake();
    }

    this.translateService.onLangChange.subscribe(() =>
      this.networkService.getConnectionTranslations()
    );
  }

  trackView(pageTitle: string): void {
    if (this.isNativeDevice) {
      // this.googleAnalytics.trackView(pageTitle)
      console.log('TODO Track', pageTitle);
    }
  }

  async setTitle(title: string = 'Radio Kasoot'): Promise<void> {
    const pageTitle = `${title} - Radio Kasoot First Haryanvi Radio`;
    this.title.setTitle(pageTitle);
    this.trackView(pageTitle);
  }

  async openSystemORBrowser(
    scheme: string,
    visitUrl: string,
    website: string
  ): Promise<void> {
    if (!this.isNativeDevice) {
      this.openInBrowser(website);
      return;
    }

    const { value: canOpen } = await AppLauncher.canOpenUrl({ url: scheme });

    if (canOpen) {
      await AppLauncher.openUrl({ url: visitUrl });
    } else {
      this.openInBrowser(website);
    }
  }

  openInBrowser(url: string): void {
    Browser.open({ url: encodeURI(url) });
  }
}
