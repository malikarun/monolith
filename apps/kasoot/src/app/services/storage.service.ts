import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({ providedIn: 'root' })
export class StorageService {
  async set(key: string, data: Record<string, any>) {
    await Storage.set({
      key,
      value: JSON.stringify(data),
    });
  }

  async get(key: string) {
    const { value } = await Storage.get({ key });
    return JSON.parse(String(value));
  }
}
