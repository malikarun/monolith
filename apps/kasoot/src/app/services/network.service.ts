import { Injectable } from '@angular/core';
import { Network } from '@capacitor/network';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { MusicService } from './music.service';

export interface IConnection {
  type: string;
}

const getValue = (vals: Record<string, string>, key: string) =>
  vals[key] === key ? '' : vals[key];

@Injectable({ providedIn: 'root' })
export class NetworkService {
  // disconnection
  private disconnectionAlert!: HTMLIonAlertElement;
  private disconnectionTitle!: string;
  private disconnectionMessage!: string;
  private disconnectionButton!: string;

  constructor(
    private alertCtrl: AlertController,
    private translateService: TranslateService,
    private musicService: MusicService
  ) {}

  async init(): Promise<void> {
    // get connection translations
    this.getConnectionTranslations();

    // init network status
    this.startTrackingNetworkStatus();
  }

  getConnectionTranslations(): void {
    const translationKeys: Array<string> = [
      'ALERTS.DISCONNECTION_TITLE',
      'ALERTS.DISCONNECTION_MESSAGE',
      'ALERTS.DISCONNECTION_BUTTON',
    ];

    this.translateService
      .get(translationKeys)
      .subscribe((vals: Record<string, string>) => {
        this.disconnectionTitle = getValue(vals, 'ALERTS.DISCONNECTION_TITLE');
        this.disconnectionMessage = getValue(
          vals,
          'ALERTS.DISCONNECTION_MESSAGE'
        );
        this.disconnectionButton = getValue(
          vals,
          'ALERTS.DISCONNECTION_BUTTON'
        );
      });
  }

  startTrackingNetworkStatus(): void {
    Network.addListener('networkStatusChange', (status) => {
      console.log('Network status changed', status);
      if (status.connectionType === 'none') {
        this.onNetworkDisconnect();
      } else {
        this.onNetworkConnect();
      }
    });
  }

  private async onNetworkConnect() {
    // watch network for a connection
    if (!this.musicService.manuallyPaused && this.musicService.playedOnce) {
      this.musicService.play(true);
    }

    if (this.disconnectionAlert) {
      this.disconnectionAlert.dismiss();
    }
  }

  private async onNetworkDisconnect() {
    // watch network for a connection
    this.musicService.play(false);

    if (this.disconnectionAlert) {
      this.disconnectionAlert.dismiss();
    }

    this.disconnectionAlert = await this.alertCtrl.create({
      header: this.disconnectionTitle,
      message: this.disconnectionMessage,
      buttons: [this.disconnectionButton],
    });

    if (this.disconnectionTitle) {
      await this.disconnectionAlert.present();
    }
  }
}
